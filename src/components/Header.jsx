import styles from "./header.module.css"


function Header(){
    return (
        <>
        <div className={styles.header}>
            
            <section className={styles.topo}>
                <img src="src/assets/logo.svg" alt="logo"></img>
                <div>
                    <img src="src/assets/facebook.svg" alt="icone facebook"></img>
                    <img src="src/assets/instagram.svg" alt="icone instagram"></img>
                </div>
            </section>
            <section className={styles.meio}>
            <img src="src/assets/lata.svg" alt="lata de guarana"></img>
                <div>
                    <h1>Aqui um título<br></br> de duas linhas</h1>
                    <button type="submit">Aqui um CTA</button>
                </div>
            </section>
        </div>
        </>
    )
}

export default Header 
