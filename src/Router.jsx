import { Routes, Route } from "react-router-dom"
import { BaseLayout } from "./BaseLayout";
import { Home } from "./Home-page/Home";

export function Router() {
    return (
        <Routes>
            <Route path="/" element={<BaseLayout></BaseLayout>}>
                <Route path="/" element={<Home />}/>               
            </Route>
        </Routes>
    )
}