import { useState, useEffect } from "react";
import styles from "./home.module.css"

export function Home() {
    return (
        <>
        <div className={styles.fundo1}>
            <section className={styles.produtos}>
                <div>
                    <h1>Produtos</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br></br> Mauris semper, tortor sed vehicula.</p>
                </div>
                <div>
                    <section className={styles.box}>
                        <img src="src/assets/garrafa.svg" alt="garrafa de guarana"></img>
                        <div>
                            <h2>Garrafa 350ml</h2>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                        <button>Aqui um CTA</button>
                    </section>
                    <section className={styles.box}>
                        <img src="src/assets/lata-100ml.svg" alt="lata de 100ml"></img>
                        <div>
                            <h2>Garrafa 100ml</h2>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                        <button>Aqui um CTA</button>
                    </section>
                </div>
            </section>
            <section className={styles.fotos}>
                <img src="src/assets/bebida-mulher.svg" alt="mulheres bebendo"></img>
                <div>
                    <div className={styles.foto_cima}>
                        <h2>“Lorem ipsum dolor sit amet, consectetur <br></br>adipiscing elit. Etiam commodo hendrerit.”</h2>
                        <p>@danielmaciel</p>
                        <div className={styles.barra}>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div className={styles.foto_baixo}>
                        <h1>Peça para seu bar</h1>
                    </div>
                </div>
            </section>
        </div>
        <div className={styles.fundo2}>
            <img src="src/assets/lata.svg"></img>
            <div>
                <h1>Quer experimentar?</h1>
                <button>Aqui um CTA</button>
            </div>
            <img src="src/assets/lata.svg"></img>
        </div>
        </>
    )
}