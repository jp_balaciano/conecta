import styles from "./footer.module.css"


function Footer() {
    return (
        <>
        <div className={styles.footer}>
            <section className={styles.titulo}>
                <h1>Fale Conosco.</h1>
                <div>
                    <img src="src/assets/facebook.svg" alt="icone facebook"></img>
                    <img src="src/assets/instagram.svg" alt="icone instagram"></img>
                    <p>|  REDES SOCIAS</p>
                </div>
            </section>
            <section className={styles.form}>
                <div>
                    <label>NOME</label>
                    <input type="text" required></input>
                </div>
                <section className={styles.info}>
                    <div>
                        <label>TELEFONE</label>
                        <input type="tel" required placeholder="(21) 00000-0000"></input>
                    </div>
                    <div>
                        <label>E-MAIL</label>
                        <input type="email" required placeholder="email@email.com.br"></input>
                    </div>
                </section>
                <div>
                    <label>ASSUNTO</label>
                    <select>
                        <option>Selecione um Assunto</option>
                    </select>
                </div>
                <div>
                    <label>MENSAGEM</label>
                    <textarea></textarea> 
                </div>
                <button type="submit">Aqui um CTA</button>
            </section>
        </div>
        <div className={styles.rodape}>
            <p>Todos os direitos reservados © 2020 Conecta</p>
            <div>
                <p>PROJETADO POR</p>
                <img src="src/assets/logo.svg" alt="logo"></img>
            </div>
        </div>
        </>
    )
}

export default Footer 